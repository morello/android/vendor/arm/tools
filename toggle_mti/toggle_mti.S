/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <asm/unistd.h>

.globl _start
_start:
	// Trigger the ToggleMTI plugin, to toggle MTI callbacks on/off.
	hlt	#0x1

	mov	x0, #0
	mov	x8, #__NR_exit
	svc	#0
